from neomodel import StructuredNode, StringProperty, ArrayProperty, UniqueProperty, RelationshipTo, RelationshipFrom


class Device(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    # Group, Number, Contact, DateTime, Rollershutter, Color, Dimmer, Switch, Player
    type_ = StringProperty(required=True)
    state = StringProperty(required=False)
    location = RelationshipTo('Location', 'LOCATION')
    # tags = ArrayProperty(required=False)
    # quantity_type: StringProperty(required=False)
    # label: StringProperty(required=False)
    # category: StringProperty(required=False)
    # tags: StringProperty(required=False)
    # group_names: StringProperty(required=False)
    # group_type: StringProperty(required=False)
    # function_name: StringProperty(required=False)
    # function_params: ArrayProperty(required=False)


class Location(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    label = StringProperty(required=False)
    device = RelationshipFrom('Device', 'LOCATION')
