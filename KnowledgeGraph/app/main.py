import json
from typing import Union, Type, Optional

from fastapi import FastAPI, status
from openhab.items import Item

from iot_platform import IoTPlatform
from neomodel import config, db
from neoORM import Device, Location
from pydantic import BaseModel
from const import oh_base_url, oh_username, oh_password, neo_bolt_uri

import uvicorn

app = FastAPI()


class ItemModel(BaseModel):
    name: str
    type: str  # Group, Number, Contact, DateTime, Rollershutter, Color, Dimmer, Switch, Player
    # quantity_type: Optional[str] = None
    # label: Optional[str] = None
    # category: Optional[str] = None
    # tags: Optional[list[str]] = None
    # group_names: Optional[list[str]] = None
    # group_type: Union[str, None] = None
    # function_name: Optional[str] = None
    # function_params: Optional[list[str]] = None


def from_oh_to_neo():
    items = oh.get_all_items().items()
    for name, item in items:
        if item.type_ == 'Location':
            Location(name=name, label=item.label).save()
            continue
        device = Device.nodes.get_or_none(name=name)
        if device is None:
            device = Device(name=name, type_=item.type_, state=item.state).save()
        else:
            device.type_ = item.type_
            device.state = item.state
        if len(item.tags) == 1:
            location = Location.nodes.get_or_none(name=item.tags[0])  # can be more than one tag so this may not work
            if location is None:
                location = Location(name=item.tags[0]).save()
            device.location.connect(location)
        device.save()


@app.get("/")
async def root():
    delete_all_nodes_and_relationships()
    from_oh_to_neo()
    return {"message": "Hello World"}


@app.get("/oh/rules")
async def changed_dimmer():
    return oh.get_rules()


@app.get("/changed")
async def changed_dimmer():
    print("313")


@app.get("/oh/items/")
async def get_openhab_items():
    lt = oh.get_all_items()
    res = {}
    for name, item in lt.items():
        res[name] = item.type_
    return res


@app.get("/neo/locations/")
async def get_neo4j_locations():
    lt = Location.nodes.all()
    res = {}
    for location in lt:
        devices = []
        for d in location.device.all():
            devices.append({'name': d.name, 'type_': d.type_})
        res[location.name] = {'label': location.label, 'devices': devices}
    return res


@app.get("/neo/items/")
async def get_neo4j_items():
    lt = Device.nodes.all()
    res = {}
    for device in lt:
        res[device.name] = {'type_': device.type_, 'state': device.state}
    return res


@app.get("/oh/items/{item_name}/")
async def get_item_info(item_name):
    res = oh.get_item_raw(item_name)
    return res


@app.get('/oh/items/{item_name}/state')
async def get_item_state(item_name):
    item = oh.get_item(item_name)
    state = item.state
    type_ = item.type_
    return {'state': state, 'type': type_}


@app.post("/oh/items/", status_code=status.HTTP_201_CREATED)
async def create_openhab_item(item_model: ItemModel):
    oh.create_item(item_model.__dict__)


@app.get('/oh/updated/')
async def updated():
    print("Updated [+]")


def delete_all_nodes_and_relationships():
    db.cypher_query(
        '''
        MATCH (n)
        DETACH DELETE n
        '''
    )


if __name__ == '__main__':
    oh = IoTPlatform(oh_base_url, username=oh_username, password=oh_password)  # BasicAuth enabled
    config.DATABASE_URL = neo_bolt_uri
    uvicorn.run(app, host="localhost", port=8082)
