import logging

from neo4j import GraphDatabase
from neo4j.exceptions import ServiceUnavailable

version = '4.4.1'


class NeoGraph:
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    @staticmethod
    def enable_log(level, output_stream):
        handler = logging.StreamHandler(output_stream)
        handler.setLevel(level)
        logging.getLogger("neo4j").addHandler(handler)
        logging.getLogger("neo4j").setLevel(level)

    def create_and_locate_device(self, device_name, location_name):
        with self.driver.session() as session:
            result = session.write_transaction(
                self._create_and_locate_device, device_name, location_name)
            for row in result:
                print("Created device {d} in location:  {l} "
                    .format(
                        d=row['d'],
                        l=row["l"]
                ))

    @staticmethod
    def _create_and_locate_device(tx, device_name, location_name):
        query = (
            "CREATE (d:Device { name: $device_name }) "
            "CREATE (l:Location { name: $location_name }) "
            "CREATE (l)-[r:location]->(d) "
            "RETURN d, l"
        )
        result = tx.run(query, device_name=device_name, location_name=location_name)
        try:
            return [{
                "d": row["d"]["name"],
                "l": row["l"]["name"],
            } for row in result]
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

    @staticmethod
    def get_graph(tx):
        result = tx.run("MATCH (a)-[r]->(b) RETURN a,r,b")
        return result.single()[0]

    def find_device(self, device_name):
        with self.driver.session() as session:
            result = session.read_transaction(self._find_and_return_device, device_name)
            for row in result:
                print("Found device: {row}".format(row=row))

    @staticmethod
    def _find_and_return_device(tx, device_name):
        query = (
            "MATCH (d:Device) "
            "WHERE d.name = $device_name "
            "RETURN d.name AS name"
        )
        result = tx.run(query, device_name=device_name)
        return [row["name"] for row in result]
