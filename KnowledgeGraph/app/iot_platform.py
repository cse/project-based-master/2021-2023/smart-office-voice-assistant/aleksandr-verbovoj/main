from openhab import OpenHAB


class IoTPlatform:
    def __init__(self, base_url, username=None, password=None):
        self.openhab = OpenHAB(base_url=base_url, username=username, password=password)

    def get_all_items(self):
        return self.openhab.fetch_all_items()

    def get_item(self, item_name):
        return self.openhab.get_item(item_name)

    def get_item_raw(self, item_name):
        return self.openhab.get_item_raw(item_name)

    def get_rules(self):
        rules = self.openhab.rules
        return {'rules': rules.get()}

    def create_item(self, item: dict):
        self.openhab.create_or_update_item(name=item['name'],
                                           _type=item['type'],
                                           quantity_type=item['quantity_type'],
                                           label=item['label'],
                                           category=item['category'],
                                           tags=item['tags'],
                                           group_names=item['group_names'],
                                           function_name=item['function_name'],
                                           function_params=item['function_params'])